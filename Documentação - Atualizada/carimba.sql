DROP SCHEMA IF EXISTS carimba;
CREATE SCHEMA IF NOT EXISTS carimba;
-- Modelo Físico
CREATE TABLE carimba.timee(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(100) NOT NULL
);

CREATE TABLE carimba.pont(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	tAdv VARCHAR(100) NOT NULL,
	carim VARCHAR(50) NOT NULL
);

CREATE TABLE carimba.usuario(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,    
    nome VARCHAR(100) NOT NULL,
    usuario VARCHAR(100) NOT NULL UNIQUE,
    senha VARCHAR(100) NOT NULL,
    tipo VARCHAR(12) NOT NULL,
    ncamisa VARCHAR(20),
    timeId INT(11) NOT NULL,
    FOREIGN KEY (timeId) REFERENCES timee(id)
);

CREATE TABLE carimba.DataT(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    dia INT(3) NOT NULL, 
    mes INT(5) NOT NULL,
    ano INT(5) NOT NULL
);

CREATE TABLE carimba.EnderecoJogo(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    rua VARCHAR(100) NOT NULL,
    numero INT(10) NOT NULL,
    pReferencia VARCHAR(100) NOT NULL
);

CREATE TABLE carimba.Agendar(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    timeAdv VARCHAR(100) NOT NULL,
    endId INT(11),
    dataId INT(11),
    userId INT(11),
    hora VARCHAR(30) NOT NULL,
    FOREIGN KEY (endId) REFERENCES EnderecoJogo(id),
    FOREIGN KEY (dataId) REFERENCES DataT(id),
    FOREIGN KEY (userId) REFERENCES usuario(id)
);

INSERT INTO carimba.timee(nome) VALUES("padrao");